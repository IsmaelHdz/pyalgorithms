
def remove_spaces(word):
    """
    Removing Spaces from given String
    Time Complex =O(n)
    Space Complex O(n)
    """
    num_spaces = 0
    word_list = list(word)

    for counter, letter in enumerate(word_list):
        if letter.isspace():
            num_spaces += 1
        else:
            word_list[counter - num_spaces] = word_list[counter]

    new_word_list = word_list[:-num_spaces]
    word = ''.join(new_word_list)
    return word


def reverse_words(phrase):
    """
    Reverse Words from given String
    Time Complex =O(n)
    Space Complex O(n)
    """
    start = 0
    array_phrase = list(phrase)
    array_length = len(array_phrase)-1

    for index, value in enumerate(array_phrase):
        if value.isspace() and index > 0:
            reverse(array_phrase, start, index - 1)
            start = index + 1

        elif index == array_length:
            reverse(array_phrase, start, index)

    reverse(array_phrase, 0, array_length)
    return array_phrase


def reverse(phrase, start, end):
    """
    This method takes each word of the string and reducing
    in each iteration
    """
    while start < end:
        phrase[start], phrase[end] = phrase[end], phrase[start]
        start += 1
        end -= 1

# def swap(phrase, start, end):
# phrase[start], phrase[end] = phrase[end], phrase[start]
# Java Style
# tmp = phrase[start]
# phrase[start] = phrase[end]
# phrase[end] = tmp


def is_palindrome(word):
    """
   This method receive as Parameter the word and Return if this word is palindrome or not
    """
    output = True
    word = list(word)
    start = 0
    end = len(word)-1

    while start < end:
        if word[start] != word[end]:
            output = False
        start += 1
        end -= 1
    return output


if __name__ == "__main__":
    no_spaces = remove_spaces(' hello world ')
    print(no_spaces)
    new_phrase = reverse_words('Hi World')
    print(new_phrase)
    is_palindrome_v = is_palindrome('radar ')
    print(is_palindrome_v)
