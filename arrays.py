def selection_sort(array):
    """Return a Sorted List using Selection Sort Algorithm."""
    for i in range(len(array)):
        min_index = i
        for j in range(i+1, len(array)):
            if array[min_index] > array[j]:
                min_index = j
        array[i], array[min_index] = array[min_index], array[i]
    return array


if __name__ == "__main__" :
    array_demo = [12, 35, 87, 26, 9, 28, 7]
    for index in selection_sort(array_demo):
        print(index)
